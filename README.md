# RPC Runner

Simple Rust application that lets you start a presence for a detectable Discord game.

## Why?

Because running games under Wine on Linux aren't detected by Discord, and thus
don't have their detectable presence that Discord usually provides.

## How?

It just uses an API endpoint to get a list of all applications, then uses regular RPC/Presence/whatever they call it now 
(gamesdk smth??) with the client ID provided by that API endpoint.

## Better Project?

I'm sure someone has done this before, and has a better program than what this crappy made-in-10-minutes code is, but I don't
know what that project is. Feel free to open an issue if you find such an app.

I do plan on making my own app that detects which applications are running under Wine prefixes and does what this program does.
I'll add it to my Gitlab/GitHub once I have motivation to do it (aka soon:tm: hopefully no promises <3).