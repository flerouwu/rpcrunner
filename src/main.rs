use std::process::exit;

use discord_presence::{Client, Event};
use inquire::Select;
use label_logger::{error, info, log, success, warn};
use label_logger::console::{Color, style};
use label_logger::OutputLabel::Custom;
use serde::Deserialize;

fn main() {
    log!(label: Custom(style("Http").fg(Color::Magenta)), "Fetching detectable applications...");
    let res = reqwest::blocking::get("https://discord.com/api/v10/applications/detectable");
    if res.is_err() {
        error!("Something went wrong! {:?}", res.err());
        exit(1);
    }

    let apps_res = res.unwrap().json::<Vec<Application>>();
    if apps_res.is_err() {
        error!("Something went wrong! {:?}", apps_res.err());
        exit(2);
    }
    let apps = apps_res.unwrap();

    // Ask user for the application
    let names: Vec<String> = apps.iter().map(|app| app.name.clone()).collect();
    let inquire_res = Select::new("Search for an app.", names).prompt();
    if inquire_res.is_err() {
        error!("Something went wrong! {:?}", inquire_res.err());
        exit(3);
    }

    // Find app by it's name
    let selected_name = inquire_res.unwrap();
    let app_opt = apps.iter().find(|app| app.name.eq_ignore_ascii_case(&selected_name));
    if app_opt.is_none() {
        error!("Unable to find the app requested!");
        exit(4);
    }
    let app = app_opt.unwrap();

    // Start presence
    info!("Starting presence for {} ({}). Press CTRL+C to exit.", app.name, app.id);
    let mut drpc = Client::new(app.id.parse().unwrap());

    drpc.on_ready(|_ctx| {
        success!("Connected to Discord!");
    });

    let thread = drpc.start();
    log!(label: Custom(style("Debug").fg(Color::Magenta)), "Waiting for Ready event.");
    drpc.block_until_event(Event::Ready).expect("failed to wait for ready event");
    assert!(Client::is_ready());

    // Set activity
    drpc.set_activity(|act| act).expect("unable to set activity");
    success!("Set activity! Check your presence.");

    // Set ctrlc shutdown
    if let Err(err) = ctrlc::try_set_handler(move || {
        info!("Exiting...");
        drpc.clear_activity().unwrap();
        exit(0);
    }) {
        warn!("Failed to set Ctrl+C handler. Client may not disconnect properly.");
        warn!("{:?}", err);
    }

    // Join thread
    info!("Joining client thread. Application will not exit until the client has closed.");
    let _ = thread.join();
}

#[derive(Default, Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Application {
    #[serde(default)]
    pub executables: Vec<Executable>,
    pub hook: bool,
    pub id: String,
    pub name: String,
    #[serde(default)]
    pub aliases: Vec<String>,
    pub overlay: Option<bool>,
    #[serde(rename = "overlay_compatibility_hook")]
    pub overlay_compatibility_hook: Option<bool>,
    #[serde(rename = "overlay_methods")]
    pub overlay_methods: Option<i64>,
}

#[derive(Default, Debug, Clone, PartialEq, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Executable {
    #[serde(rename = "is_launcher")]
    pub is_launcher: bool,
    pub name: String,
    pub os: String,
    pub arguments: Option<String>,
}
